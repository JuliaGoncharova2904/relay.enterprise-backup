﻿using MomentumPlus.Relay.Authorization.Auth;
using MomentumPlus.Relay.Authorization.Models;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using MomentumPlus.Relay.Models;
using LoginViewModel = MomentumPlus.Relay.Authorization.Models.LoginViewModel;
using ResetPasswordViewModel = MomentumPlus.Relay.Authorization.Models.ResetPasswordViewModel;
using MomentumPlus.Relay.Authorization.Domain.Entities;
using System.Linq;
using MomentumPlus.Relay.Mailer;

namespace MomentumPlus.Relay.Authorization.Controllers
{
    public class AccountController : BaseController
    {
        public AccountController()
        { }

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager) : base(userManager, signInManager)
        { }

        /// <summary>
        /// Render Main Page
        /// </summary>
        [HttpGet]
        [AllowAnonymous]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }

            return RedirectToAction("Login");
        }

        /// <summary>
        /// Render login view or redirect to home if already logged in.
        /// </summary>
        /// <param name="returnUrl">URL to return to on successful login</param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Login(string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }

            return View(new LoginViewModel { ReturnUrl = returnUrl });
        }

        /// <summary>
        /// Handle login form submission
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var result = await SignInManager.PasswordSignInAsync(model.UserName, model.Password, model.RememberMe, shouldLockout: false);

            Session.Clear();

            switch (result)
            {
                case SignInStatus.Success:
                    return Redirect(Url.IsLocalUrl(model.ReturnUrl) ? model.ReturnUrl : "/");
                case SignInStatus.LockedOut:
                    ModelState.AddModelError("Message", "The user account is locked. Please speak to your iHandover Consultant.");
                    break;
                case SignInStatus.Failure:
                    ModelState.AddModelError("Message", "The user name or password provided is incorrect.");
                    break;
            }

            return View(model);
        }

        /// <summary>
        /// Log off current user and redirect to login page
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            Session.Clear();

            return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// Render forgotten password form
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public ActionResult ForgottenPassword()
        {
            return View(new ForgotPasswordViewModel());
        }

        /// <summary>
        /// Render forgotten password form
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ForgottenPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                ApplicationUser user = UserManager.FindByName(model.Email);

                if (user != null)
                {
                    var resetUrl = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Action("ResetPassword", new { resetPasswordHash = user.SecurityStamp }));


                    UserManager.SendEmail(user.Id, "iHandover RelayWorks Password Reset",
                        "Hi. <br /> Someone requested a password reset for your iHandover RelayWorks account. <br /> If you did not request this email please ignore it. <br /> If you did request this email click the URL below to reset your password: <a href=\"" + resetUrl + "\">link</a>");


                    AddAlert(AlertType.Success, "A password reset email has been sent to your email account, please check your email to continue the process.");

                    return RedirectToAction("Login");
                }

                ModelState.AddModelError("Email", "No user with that email address exists.");
            }

            return View(model);
        }


        [HttpGet]
        [AllowAnonymous]
        public ActionResult ResetPassword(string resetPasswordHash)
        {
            ApplicationUser user = UserManager.Users.SingleOrDefault(u => u.SecurityStamp == resetPasswordHash);

            return (user == null) 
                    ? (ActionResult)RedirectToAction("Login")
                    : (ActionResult)View(new ResetPasswordViewModel { PasswordResetGuid = resetPasswordHash });
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPassword(ResetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                ApplicationUser user = UserManager.Users.SingleOrDefault(u => u.SecurityStamp == model.PasswordResetGuid);

                if (user != null && UserManager.RemovePassword(user.Id).Succeeded && UserManager.AddPassword(user.Id, model.NewPassword).Succeeded)
                {
                    AddAlert(AlertType.Success, "Password changed successfully, you may now log in.");
                }

                return RedirectToAction("Login");
            }

            return View(model);
        }

    }
}