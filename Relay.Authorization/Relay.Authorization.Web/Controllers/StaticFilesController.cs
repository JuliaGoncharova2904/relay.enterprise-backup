﻿using System.Net;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Authorization.Controllers
{
    public class StaticFilesController : Controller
    {
        // GET: StaticFiles
        public ActionResult Index(string fileName)
        {
            string fileContent = string.Empty;

            switch(fileName)
            {
                case "registration.js":
                    //fileContent = FileResources.registration;
                    break;
                case "multi-tenant.css":
                    //fileContent = FileResources.multi_tenant;
                    break;
                default:
                    return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            return Content(fileContent);
        }
    }
}