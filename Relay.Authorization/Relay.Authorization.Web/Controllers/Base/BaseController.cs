﻿using System.Collections.Generic;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System.Web.Mvc;
using MomentumPlus.Relay.Authorization.Auth;
using System.Web;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.Authorization.Controllers
{
    [Authorize]
    public abstract class BaseController : Controller
    {
        private readonly ApplicationUserManager _userManager;
        private readonly ApplicationSignInManager _signInManager;


        protected BaseController()
        { }

        protected BaseController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            this._userManager = userManager;
            this._signInManager = signInManager;
        }

        protected ApplicationSignInManager SignInManager => _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();

        protected ApplicationUserManager UserManager => _userManager ?? HttpContext.GetOwinContext().Get<ApplicationUserManager>();

        protected IAuthenticationManager AuthenticationManager => HttpContext.GetOwinContext().Authentication;

        #region Alerts

        /// <summary>
        /// Add an alert to be shown on the next page loaded.
        /// </summary>
        /// <param name="type">The type of the alert.</param>
        /// <param name="message">Alert message.</param>
        protected void AddAlert(AlertType type, string message)
        {
            var alert = new AlertViewModel
            {
                Type = type,
                Message = message
            };

            List<AlertViewModel> alerts = TempData.ContainsKey("Alerts")
                ? (List<AlertViewModel>)TempData["Alerts"]
                : new List<AlertViewModel>();

            alerts.Insert(0, alert);

            TempData["Alerts"] = alerts;
        }

        #endregion


    }
}