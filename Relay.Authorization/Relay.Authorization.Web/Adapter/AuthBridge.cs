﻿using Owin;
using MomentumPlus.Relay.Interfaces.Auth;
using MomentumPlus.Relay.Authorization.Auth;
using MomentumPlus.Relay.Authorization.Adapter;
using System.Configuration;
using System;
using System.Collections.Generic;

namespace MomentumPlus.Relay.Auth
{
    public class AuthBridge : IAuthBridge
    {
        private IAuthService _authService;

        public string GetActiveRelayDbConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["MomentumContext"].ConnectionString;
        }

        public void RegisterAuthModule(IAppBuilder app)
        {
            AuthModuleConfiguration.Apply(app);
        }

        public IEnumerable<string> GetAllRelayDbConnectionStrings()
        {
            return new[] { this.GetActiveRelayDbConnectionString() };
        }

        public IAuthService AuthService => _authService ?? (_authService = new AuthService());

        public IRestrictionProvider RestrictionProvider => null;
    }
}