﻿using MomentumPlus.Relay.Authorization.Auth;
using MomentumPlus.Relay.Interfaces.Auth;
using System;
using Microsoft.AspNet.Identity.Owin;
using System.Collections.Generic;
using System.Web;
using MomentumPlus.Relay.Roles;
using System.Linq;
using MomentumPlus.Relay.Authorization.Domain.Entities;
using Microsoft.AspNet.Identity;
using MomentumPlus.Relay.Authorization.Domain;
using System.Data.Entity;
using System.Globalization;
using System.Net.Mail;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.Authorization.Adapter
{
    public class AuthService : IAuthService
    {
        private EnterpriseContext DbContext => HttpContext.Current.GetOwinContext().Get<EnterpriseContext>();

        private ApplicationRoleManager RoleManager =>
            HttpContext.Current.GetOwinContext().Get<ApplicationRoleManager>();

        private ApplicationUserManager UserManager =>
            HttpContext.Current.GetOwinContext().Get<ApplicationUserManager>();

        private ApplicationSignInManager SignInManager =>
            HttpContext.Current.GetOwinContext().Get<ApplicationSignInManager>();

        public void SendMail(Guid userId, string userName, string urlUdtatePassword)
        {
            UserManager.SendEmail(userId, "Welcome to RelayWorks",
                " Hello, " + userName + "! <br />" +
                " You have been signed up to RelayWorks. <br />" +
                " To complete your account setup, please follow the link below and update your password:  <a href=\"" +
                urlUdtatePassword + "\">link</a>" +
                " <br /> Many Thanks, <br /> " +
                " RelayWorks Team");
        }


        public void SendAutoRenewSubscriptionMail(Guid userId, string emailOfSubscription)
        {
            var from = "noreply@ihandover.co";
            var pass = "Ball00n$%";

            SmtpClient client = new SmtpClient("smtp.gmail.com", 587)
            {
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new System.Net.NetworkCredential(@from, pass),
                EnableSsl = true
            };

            var mail = new MailMessage(from, "info@ihandover.co")
            {
                Subject = "Subscription Cancellation",
                Body = "iHandover Team, <br />" +
                       "The subscription " + emailOfSubscription + " has turned the auto-renew billing off.<br />" +
                       "Kind Regards, <br /> " +
                       " RelayWorks",
                IsBodyHtml = true
            };

            client.Send(mail);
        }

        public string GetSecurityStampForUpdatePassword(Guid userId)
        {
            return UserManager.FindById(userId).SecurityStamp;
        }


        public bool AddUserToRole(Guid userId, string role)
        {
            bool result = true;
            ApplicationUser user = UserManager.FindById(userId);

            using (DbContextTransaction transaction = DbContext.Database.BeginTransaction())
            {
                result = UserManager.AddToRole(user.Id, role).Succeeded;
                transaction.Commit();
            }

            return result;
        }

        public bool RemoveUserFromRole(Guid userId, string role)
        {
            bool result = true;
            ApplicationUser user = UserManager.FindById(userId);

            using (DbContextTransaction transaction = DbContext.Database.BeginTransaction())
            {
                result = UserManager.RemoveFromRole(user.Id, role).Succeeded;
                transaction.Commit();
            }

            return result;
        }

        public IEnumerable<string> GetUserRoles(Guid userId)
        {
            return UserManager.GetRoles(userId);
        }

        public IEnumerable<string> GetAllRolesAccessedByUser(Guid userId)
        {
            return new[]
            {
                iHandoverRoles.Relay.Administrator,
                iHandoverRoles.Relay.iHandoverAdmin,
                iHandoverRoles.Relay.HeadLineManager,
                iHandoverRoles.Relay.LineManager,
                iHandoverRoles.Relay.SafetyManager,
                iHandoverRoles.Relay.Contributor,
                iHandoverRoles.Relay.User
            };
        }

        public IEnumerable<Guid> GetUsersIdsByRole(string role, Guid requestOwnerId)
        {
            return RoleManager.FindByName(role).Users.Select(u => u.UserId);
        }

        public IEnumerable<Guid> GetUsersIdsByRoles(IEnumerable<string> roles, Guid requestOwnerId)
        {
            List<Guid> usersIds = new List<Guid>();

            foreach (string role in roles)
            {
                usersIds.AddRange(RoleManager.FindByName(role).Users.Select(u => u.UserId));
            }

            return usersIds.Distinct();
        }

        public bool CreateUser(string login, string password, bool needEmailConfirm = false)
        {
            return UserManager.Create(new ApplicationUser
            {
                Id = Guid.NewGuid(),
                UserName = login,
                Email = login,
                EmailConfirmed = needEmailConfirm
            }, password).Succeeded;
        }


        public bool RemoveUser(Guid userId, Guid initiatorId)
        {
            bool result = true;
            ApplicationUser user = UserManager.FindById(userId);
            IList<string> rolesForUser = UserManager.GetRoles(userId);

            using (DbContextTransaction transaction = DbContext.Database.BeginTransaction())
            {
                foreach (var login in user.Logins.ToList())
                {
                    UserLoginInfo userLoginInfo = new UserLoginInfo(login.LoginProvider, login.ProviderKey);
                    result = UserManager.RemoveLogin(login.UserId, userLoginInfo).Succeeded && result;
                }

                if (rolesForUser.Any())
                {
                    result = rolesForUser.Aggregate(result,
                        (current, item) => UserManager.RemoveFromRole(user.Id, item).Succeeded && current);
                }

                result = UserManager.Delete(user).Succeeded && result;

                transaction.Commit();
            }

            return result;
        }

        public bool ChangeUserLogin(Guid userId, string newLogin)
        {
            ApplicationUser user = UserManager.FindById(userId);

            user.UserName = newLogin;
            user.Email = newLogin;

            return UserManager.Update(user).Succeeded;
        }


        public bool ChangeUserPassword(Guid userId, string password, string newPassword)
        {
            bool result;

            using (DbContextTransaction transaction = DbContext.Database.BeginTransaction())
            {
                result = UserManager.ChangePassword(userId, password, newPassword).Succeeded;
                transaction.Commit();
            }

            return result;
        }

        public bool ResetUserPassword(Guid userId, string newPassword)
        {
            bool result;

            using (DbContextTransaction transaction = DbContext.Database.BeginTransaction())
            {
                result = UserManager.RemovePassword(userId).Succeeded &&
                         UserManager.AddPassword(userId, newPassword).Succeeded;
                transaction.Commit();
            }

            return result;
        }

        public void UpdateLastLoginDateForUser(Guid userId)
        {
            var user = UserManager.FindById(userId);

            user.LastLoginDate = DateTime.Now;

            UserManager.Update(user);
        }

        public SubscriptionDetailsModel GetSubscriptionDetailsModel()
        {
            throw new NotImplementedException();
        }


        public void CancelSubscriptionAtPeriodEnd(bool autoRenewSubscription)
        {
            throw new NotImplementedException();
        }

        public Guid GetUserIdBySubscriptionId(Guid? subscriptionId)
        {
            throw new NotImplementedException();
        }

        public UpgradeSubscriptionViewModel GetUpgradeSubscriptionViewModel()
        {
            throw new NotImplementedException();
        }

        public bool SubscriptionMaxUserCanChange(int newUserNumber)
        {
            return true;
        }

        public bool UpgradeSubscription(UpgradeSubscriptionViewModel model)
        {
            throw new NotImplementedException();
        }

        public Guid GetUserIdByLogin(string login)
        {
            return UserManager.FindByName(login).Id;
        }

        public DateTime? GetLastLoginDateForUser(Guid userId)
        {
            return UserManager.FindById(userId).LastLoginDate;
        }

        public void ReactivateUser(Guid userId, bool status)
        {
            UserManager.SetLockoutEnabled(userId, status);
            DateTimeOffset lockEndDate = status ? DateTimeOffset.MaxValue : DateTimeOffset.Now;
            UserManager.SetLockoutEndDate(userId, lockEndDate);
        }

        public bool IsLockUser(Guid userId)
        {
            return UserManager.IsLockedOut(userId);
        }

        public Guid? GetUserSubscriptionId(Guid userId)
        {
            throw new NotImplementedException();
        }


        public bool IsTrialSubscription(Guid userId)
        {
            return false;
        }

        public int? NumberOfRemainingTrialDays(Guid userId)
        {
            return null;
        }

        public bool ContributorEnable(Guid userId)
        {
            return true;
        }

        public RestrictionType? GetSubscriptionType(Guid userId)
        {
            return RestrictionType.Ultimate;
        }


        public string GetTimeZoneFromSubscription(Guid userId)
        {
            return null;
        }

        public bool GetAccessToPageUpgrade(Guid userId)
        {
            return false;
        }

    }
}