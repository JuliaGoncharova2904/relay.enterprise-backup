﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Relay.Authorization.Models
{
    public class ResetPasswordViewModel
    {
        public string PasswordResetGuid { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = @"New Password")]
        [RegularExpression(
            @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{8,}$",
            ErrorMessage = @"Your password must be 8 characters and contain a mixture of upper and lower-case letters, numbers and special characters such as !, #, @ or ?"
        )]
        public string NewPassword { get; set; }

        [Required]
        [Compare("NewPassword", ErrorMessage = @"The passwords do not match.")]
        [DataType(DataType.Password)]
        [Display(Name = @"Confirm New Password")]
        public string ConfirmNewPassword { get; set; }
    }
}
