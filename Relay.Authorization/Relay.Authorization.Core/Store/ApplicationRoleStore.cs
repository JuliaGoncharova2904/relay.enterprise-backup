﻿using System;
using Microsoft.AspNet.Identity.EntityFramework;
using MomentumPlus.Relay.Authorization.Domain;
using MomentumPlus.Relay.Authorization.Domain.Entities;


namespace MomentumPlus.Relay.Authorization.Auth
{
    public class ApplicationRoleStore : RoleStore<ApplicationRole, Guid, ApplicationUserRole>
    {
        public ApplicationRoleStore(EnterpriseContext context)
            : base(context)
        {
        }
    }
}
