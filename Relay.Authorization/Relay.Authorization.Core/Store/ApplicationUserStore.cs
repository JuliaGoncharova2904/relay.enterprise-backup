﻿using System;
using System.Data.Entity;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using MomentumPlus.Relay.Authorization.Domain;
using MomentumPlus.Relay.Authorization.Domain.Entities;

namespace MomentumPlus.Relay.Authorization.Auth
{
    public class ApplicationUserStore :
     UserStore<ApplicationUser, ApplicationRole, Guid, ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim>,
     IUserStore<ApplicationUser, Guid>,
     IDisposable
    {
        public ApplicationUserStore(EnterpriseContext context) : base(context)
        {
            _context = context;
        }

        private readonly EnterpriseContext _context;

        public async Task<ApplicationUser> FindByIdAsync(string userName)
        {
            return await _context.Users.Include(x => x.Roles).FirstOrDefaultAsync(n => n.UserName == userName);
        }
    }
}
