﻿using System;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using MomentumPlus.Relay.Authorization.Domain.Entities;


namespace MomentumPlus.Relay.Authorization.Auth
{
    public static class CookieProvider
    {
        public static CookieAuthenticationProvider Create()
        {
            CookieAuthenticationProvider provider = new CookieAuthenticationProvider
            {
                OnApplyRedirect = ctx =>
                {
                    if (!IsApiRequest(ctx.Request))
                    {
                        ctx.Response.Redirect(ctx.RedirectUri);
                    }
                },
                OnValidateIdentity = SecurityStampValidator
                .OnValidateIdentity<ApplicationUserManager, ApplicationUser, Guid>(
                    validateInterval: TimeSpan.FromMinutes(30),
                    regenerateIdentityCallback: (manager, user) =>
                        user.GenerateUserIdentityAsync(manager),
                     getUserIdCallback: user => Guid.Parse(user.GetUserId()))
            };

            return provider;
        }

        private static bool IsApiRequest(IOwinRequest request)
        {
            string apiPath = VirtualPathUtility.ToAbsolute("~/api/");
            return request.Uri.LocalPath.ToLower().StartsWith(apiPath);
        }
    }
}
