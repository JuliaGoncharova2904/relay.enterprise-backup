﻿using System;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using MomentumPlus.Relay.Authorization.Domain;
using MomentumPlus.Relay.Authorization.Domain.Entities;

namespace MomentumPlus.Relay.Authorization.Auth
{
    public class ApplicationRoleManager : RoleManager<ApplicationRole,Guid>
    {
        public ApplicationRoleManager(
            IRoleStore<ApplicationRole, Guid> roleStore)
            : base(roleStore)
        {
        }
        public static ApplicationRoleManager Create(
            IdentityFactoryOptions<ApplicationRoleManager> options, IOwinContext context)
        {
            return new ApplicationRoleManager(
                new RoleStore<ApplicationRole, Guid, ApplicationUserRole>(context.Get<EnterpriseContext>()));
        }
    }
}
