﻿using System;
using Microsoft.AspNet.Identity.EntityFramework;

namespace MomentumPlus.Relay.Authorization.Domain.Entities
{
    public class ApplicationUserClaim : IdentityUserClaim<Guid>
    {
        public virtual ApplicationUser User { get; set; }
    }
}
