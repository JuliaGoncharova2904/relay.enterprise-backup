﻿using System;
using Microsoft.AspNet.Identity.EntityFramework;

namespace MomentumPlus.Relay.Authorization.Domain.Entities
{
    public class ApplicationRole : IdentityRole<Guid, ApplicationUserRole>
    {
        public string Description { get; set; }
    }
}
