﻿using System.ComponentModel;

namespace MomentumPlus.Relay.Authorization.Domain.Entities
{
    public enum RestrictionType
    {
        [Description("Basic")]
        Basic = 0,

        [Description("Intermediate")]
        Premium = 1,

        [Description("Advanced")]
        Ultimate = 2
    }
}