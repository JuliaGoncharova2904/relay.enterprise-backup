﻿using System;
using Microsoft.AspNet.Identity.EntityFramework;

namespace MomentumPlus.Relay.Authorization.Domain.Entities
{
    public class ApplicationUserLogin : IdentityUserLogin<Guid>
    {
        public Guid Id { get; set; }

        public virtual ApplicationUser User { get; set; }
    }
}
