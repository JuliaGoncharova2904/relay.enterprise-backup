﻿using System.Data.Entity.Migrations;

namespace MomentumPlus.Relay.Authorization.Domain
{
    public sealed class IdentityInitializerConfiguration : DbMigrationsConfiguration<EnterpriseContext>
    {
        public IdentityInitializerConfiguration()
        {
            AutomaticMigrationsEnabled = false;
            AutomaticMigrationDataLossAllowed = false;
        }

        protected override void Seed(EnterpriseContext context)
        {
            IdentityInitializer.InitializeRoles(context);
            IdentityInitializer.InitializeUsers(context);
            IdentityInitializer.InitializeUserRoles(context);
        }

    }
}
