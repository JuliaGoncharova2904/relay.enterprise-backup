﻿using System;

namespace MomentumPlus.Relay.Authorization.Domain
{
    public static class IdentityInitializerConstants
    {
        public static class Users
        {
            public static readonly Guid iHandoverAdminUserId = new Guid("D18E8B72-1756-4CC4-BBED-855153269071");
        }
    }
}
