﻿using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using MomentumPlus.Relay.Authorization.Domain.Entities;

namespace MomentumPlus.Relay.Authorization.Domain
{

    public class IdentityInitializer : MigrateDatabaseToLatestVersion<EnterpriseContext, IdentityInitializerConfiguration>
    {
        public override void InitializeDatabase(EnterpriseContext context)
        { }

        public static void InitializeRoles(EnterpriseContext context)
        {
            string[] roles =
            {
                "Administrator",
                "iHandover Admin",
                "Line Manager",
                "User",
                "Contributor",
                "Safety Manager",
                "Head Line Manager",
                "Executive User"
            };

            foreach (string role in roles)
            {
                if (!context.Roles.Any(r => r.Name == role))
                {
                    context.Roles.AddOrUpdate(new ApplicationRole { Name = role });
                }
            }

            context.SaveChanges();
        }

        public static void InitializeUsers(EnterpriseContext context)
        {
            CreateUser(context,
                IdentityInitializerConstants.Users.iHandoverAdminUserId,
                "admin@ihandover.co",
                "admin@ihandover.co");

            context.SaveChanges();
        }


        public static void InitializeUserRoles(EnterpriseContext context)
        {
            ApplicationRole iHandoverAdminRole = context.Roles.Single(r => r.Name == "iHandover Admin");

            if (!context.ApplicationUserRoles.Any(ur => ur.UserId == IdentityInitializerConstants.Users.iHandoverAdminUserId && ur.RoleId == iHandoverAdminRole.Id))
            {
                context.ApplicationUserRoles.AddOrUpdate(new ApplicationUserRole
                {
                    UserId = IdentityInitializerConstants.Users.iHandoverAdminUserId,
                    RoleId = iHandoverAdminRole.Id
                });
            }

            context.SaveChanges();
        }


        #region Private Methods

        private static void CreateUser(EnterpriseContext context,
            Guid userId,
            string username,
            string email)
        {
            string passwordHash = "ANZtVV4oh6nqD+Zf2p4oJ1NrQXImWXsQ7o7Zt4x6BSAkdEE6IfZghguXzF4m9anuKA==";

            if (!context.Users.Any(u => u.Id == userId))
            {
                context.Database.ExecuteSqlCommand($"INSERT INTO [identity].[ApplicationUsers] (Id, UserName, PasswordHash, AccessFailedCount, Email, EmailConfirmed, SecurityStamp, PhoneNumberConfirmed, TwoFactorEnabled, LockoutEnabled) VALUES('{userId}', '{username}', '{passwordHash}', 0, '{email}', 'True', '{Guid.NewGuid().ToString()}', '', 'False', 'False')");
            }
        }

        #endregion


    }

}
