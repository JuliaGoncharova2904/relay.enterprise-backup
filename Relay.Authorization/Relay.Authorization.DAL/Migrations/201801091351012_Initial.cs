namespace MomentumPlus.Relay.Authorization.Domain
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "identity.ApplicationUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Guid(nullable: false),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("identity.ApplicationUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "identity.ApplicationUsers",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        LastLoginDate = c.DateTime(),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "identity.ApplicationUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.Guid(nullable: false),
                        Id = c.Guid(nullable: false, identity: true),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("identity.ApplicationUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "identity.ApplicationUserRoles",
                c => new
                    {
                        UserId = c.Guid(nullable: false),
                        RoleId = c.Guid(nullable: false),
                        Id = c.Guid(nullable: false, identity: true),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("identity.ApplicationRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("identity.ApplicationUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "identity.ApplicationRoles",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Description = c.String(),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
        }
        
        public override void Down()
        {
            DropForeignKey("identity.ApplicationUserRoles", "UserId", "identity.ApplicationUsers");
            DropForeignKey("identity.ApplicationUserRoles", "RoleId", "identity.ApplicationRoles");
            DropForeignKey("identity.ApplicationUserLogins", "UserId", "identity.ApplicationUsers");
            DropForeignKey("identity.ApplicationUserClaims", "UserId", "identity.ApplicationUsers");
            DropIndex("identity.ApplicationRoles", "RoleNameIndex");
            DropIndex("identity.ApplicationUserRoles", new[] { "RoleId" });
            DropIndex("identity.ApplicationUserRoles", new[] { "UserId" });
            DropIndex("identity.ApplicationUserLogins", new[] { "UserId" });
            DropIndex("identity.ApplicationUsers", "UserNameIndex");
            DropIndex("identity.ApplicationUserClaims", new[] { "UserId" });
            DropTable("identity.ApplicationRoles");
            DropTable("identity.ApplicationUserRoles");
            DropTable("identity.ApplicationUserLogins");
            DropTable("identity.ApplicationUsers");
            DropTable("identity.ApplicationUserClaims");
        }
    }
}
